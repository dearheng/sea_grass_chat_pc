import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'//首页模板
import MsgBox from '@/components/MsgBox'//首页模板
import Login from '@/components/Login'//首页模板
import ChatRecord from '@/components/ChatRecord'//首页模板

Vue.use(Router)






export default new Router({
    routes: [
        {
            path: '/index',
            name: 'Index',
            component: Index
        },
        {
            path: '/msgbox/:userId',
            name: 'MsgBox',
            component: MsgBox
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/chatrecord/:userId',
            name: 'ChatRecord',
            component: ChatRecord
        }
    ]
})
