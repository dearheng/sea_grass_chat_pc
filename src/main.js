// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import css from './css/style.css'
import Vue from 'vue'
import App from './App'//共用模板
import router from './router'//路由配置
import jquery from './assets/jquery'//jquery对象
// import Vant from 'vant';//第三方组件库
// import 'vant/lib/vant-css/index.css';//第三方组件库样式
// Vue.config.productionTip = false
// Vue.use(Vant);
import iView from 'iview';
import 'iview/dist/styles/iview.css';
Vue.use(iView);

import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'
Vue.use(contentmenu)

import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'
Vue.use(preview)
import { fn } from './js/function'//自定义方法集
Vue.prototype.$fn = fn;

//切换本地和线上环境
var locationTest = true;
Vue.prototype.URL = fn.get_URL(locationTest);

//VUE全局格式化事件过滤器
Vue.filter('formatDate', function (value,formType) {
    var formType = formType || 'yyyy-MM-dd hh:mm:ss';
    let date = new Date(value.toString().length > 10 ? value : value*1000);
    return fn.formatDate(date, formType)
})

Vue.filter('limitLength', function (str,len) {
    try{
        var str_length = 0;
        var str_len = 0;
        var str_cut = new String();
        str_len = str.length;
        for (var i = 0; i < str_len; i++) {
            var a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
                //中文字符的长度经编码之后大于4  
                str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
                str_cut = str_cut.concat("...");
                return str_cut;
            }
        }
        if (str_length < len) {
            return str;
        }
    }catch(e){

    }
})

import VueSocketio from 'vue-socket.io';
import socketio from 'socket.io-client';
Vue.use(VueSocketio, socketio(Vue.prototype.URL.wss));


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  
  watch: {
    "$route": function(data){
        if(data.path == '/'){
            this.$fn.isLogin((status)=>{
                if(status == -1){//当前未登陆
                    this.$router.push({path:'/login'})
                }
            });
        }
    }
  }
})
